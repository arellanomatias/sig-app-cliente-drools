package Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.kie.api.KieServices;
import org.kie.api.command.BatchExecutionCommand;
import org.kie.api.command.Command;
import org.kie.api.command.KieCommands;
import org.kie.api.runtime.ExecutionResults;
import org.kie.server.api.marshalling.MarshallingFormat;
import org.kie.server.api.model.ServiceResponse;
import org.kie.server.client.CredentialsProvider;
import org.kie.server.client.KieServicesClient;
import org.kie.server.client.KieServicesConfiguration;
import org.kie.server.client.KieServicesFactory;
import org.kie.server.client.RuleServicesClient;
import org.kie.server.client.credentials.EnteredCredentialsProvider;

/**
 * KIE-Server client example for the Quick Loan Bank demo that can be found here: https://github.com/jbossdemocentral/rhdm7-qlb-loan-demo
 *
 * Shows how the KIE-Server client API can be used to send data to the rules engine in KIE-Server and retrieve the results.
 * 
 * @author <a href="mailto:duncan.doyle@redhat.com">Duncan Doyle</a>
 */
public class PruebaPing {

	private static final String KIE_SERVER_URL = "http://localhost:8080/kie-server/services/rest/server";

	private static final String USERNAME = "marellano";

	private static final String PASSWORD = "megapro1";

	private static final String CONTAINER_ID ="DemoRule_1.0.0-SNAPSHOT";
	//private static final String CONTAINER_ID ="SAS_1.0.0-SNAPSHOT";
	public static void main(String[] args) {

		KieServices kieServices = KieServices.Factory.get();

		CredentialsProvider credentialsProvider = new EnteredCredentialsProvider(USERNAME, PASSWORD);

		KieServicesConfiguration kieServicesConfig = KieServicesFactory.newRestConfiguration(KIE_SERVER_URL, credentialsProvider);

		// Set the Marshaling Format to JSON. Other options are JAXB and XSTREAM
		kieServicesConfig.setMarshallingFormat(MarshallingFormat.JSON);

		KieServicesClient kieServicesClient = KieServicesFactory.newKieServicesClient(kieServicesConfig);

		// Retrieve the RuleServices Client.
		RuleServicesClient rulesClient = kieServicesClient.getServicesClient(RuleServicesClient.class);

		/*
		 * Create the list of commands that we want to fire against the rule engine. In this case we insert 2 objects, applicant and loan,
		 * and we trigger a ruleflow (with the StartProcess command).
		 */
		List<Command<?>> commands = new ArrayList();

		KieCommands commandFactory = kieServices.getCommands();
		//The identifiers that we provide in the insert commands can later be used to retrieve the object from the response.
		commands.add(commandFactory.newInsert("matias", "object"));
		commands.add(commandFactory.newFireAllRules());
		
		/*
		 * The BatchExecutionCommand contains all the commands we want to execute in the rules session, as well as the identifier of the
		 * session we want to use.
		 */
		BatchExecutionCommand batchExecutionCommand = commandFactory.newBatchExecution(commands);

		ServiceResponse<ExecutionResults> response = rulesClient.executeCommandsWithResults(CONTAINER_ID, batchExecutionCommand);

		ExecutionResults results = response.getResult();

		System.out.println("bunisimo");
	}

}
