package Test;

import gov.senasa.drools.DroolsService;
import gov.senasa.drools.DroolsServiceClient;
import gov.senasa.sas.data.dto.ServicioDTO;
import gov.senasa.sas.data.dto.TurnoDTO;
import gov.senasa.sas.model.enums.EstadoServicioEnum;

public class TestCalcularEstaadoServivio {

	
	public static void main(String[] args) {
		
		ServicioDTO servicio = new ServicioDTO();
		servicio.setEstadoServicio(EstadoServicioEnum.INICIAL);
//		servicio.setTurno(new TurnoDTO());
		System.out.println(servicio.getEstadoServicio().getText());
//		DroolsService.calcularEstado(servicio);
		System.out.println(DroolsServiceClient.calcularEstado(servicio).getEstadoServicio().getText());
		
	}

}
