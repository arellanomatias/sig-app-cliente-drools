package gov.senasa.sas.data.dto;

public class ItemFormularioDTO {
	private Long id;
	private int orden;
	private String etiqueta;
	private boolean obligatorio;
	private Long idTipoDato;
	private String nombreTipoDato;
	private String tipoDato;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getOrden() {
		return orden;
	}
	public void setOrden(int orden) {
		this.orden = orden;
	}
	public String getEtiqueta() {
		return etiqueta;
	}
	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}
	public boolean isObligatorio() {
		return obligatorio;
	}
	public void setObligatorio(boolean obligatorio) {
		this.obligatorio = obligatorio;
	}
	public Long getIdTipoDato() {
		return idTipoDato;
	}
	public void setIdTipoDato(Long idTipoDato) {
		this.idTipoDato = idTipoDato;
	}
	public String getNombreTipoDato() {
		return nombreTipoDato;
	}
	public void setNombreTipoDato(String nombreTipoDato) {
		this.nombreTipoDato = nombreTipoDato;
	}
	public String getTipoDato() {
		return tipoDato;
	}
	public void setTipoDato(String tipoDato) {
		this.tipoDato = tipoDato;
	}
}