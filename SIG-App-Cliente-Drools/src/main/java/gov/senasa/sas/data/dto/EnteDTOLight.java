package gov.senasa.sas.data.dto;

public class EnteDTOLight {
	protected Long id;	
	protected String identificador;
	protected String razonSocial;
	protected String tipoEnte;
	protected Boolean baja;
	
	public EnteDTOLight() {
		super();
	}

	public EnteDTOLight(Long id, String identificador, String razonSocial, String tipoEnte) {
		super();
		this.id = id;
		this.identificador = identificador;
		this.razonSocial = razonSocial;
		this.tipoEnte = tipoEnte;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public String getTipoEnte() {
		return tipoEnte;
	}
	public void setTipoEnte(String tipoEnte) {
		this.tipoEnte = tipoEnte;
	}
	public Boolean getBaja() {
		return baja;
	}
	public void setBaja(Boolean baja) {
		this.baja = baja;
	}
}