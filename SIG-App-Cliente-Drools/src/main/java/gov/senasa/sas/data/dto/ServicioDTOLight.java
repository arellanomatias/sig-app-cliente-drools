package gov.senasa.sas.data.dto;

import java.util.ArrayList;
import java.util.List;

public class ServicioDTOLight {

	private Long id;
	private EnteDTOLight ente;
//	private Long idEnte;
	private Integer idOficina;
	private String nombreOficina;
//	private String lugar;
//	private TurnoDTO turno;
	private String estadoServicio;
	protected String fechaAlta;
	protected String usuarioAlta;
//	protected String observacion;
//	protected TramiteDTO tramite;
//	private Long idTramite;
//	private String telefono;
//	private String email;
//	private Boolean enviarNovedades;
//	private String detalleTramite;
//	private Float latitud;
//	private Float longitud;
//	private String descripcionLugar;
	private List<FormularioDTO> formularios;
	private String sistema;
	private String tipoDeServicio;
	private List<PersonaSENASADTOLight> agentes;
	
	
	public ServicioDTOLight() {
		super();
	}

	public ServicioDTOLight(Long id, Long idEnte, String identificador, String razonSocial, String tipo, String nombreOficina) {
		super();
		this.id = id;
		this.ente = new EnteDTOLight(idEnte, identificador, razonSocial, tipo);
		this.nombreOficina = nombreOficina;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public EnteDTOLight getEnte() {
		return ente;
	}
	public void setEnte(EnteDTOLight ente) {
		this.ente = ente;
	}
	public Integer getIdOficina() {
		return idOficina;
	}
	public void setIdOficina(Integer idOficina) {
		this.idOficina = idOficina;
	}
	public String getNombreOficina() {
		return nombreOficina;
	}
	public void setNombreOficina(String nombreOficina) {
		this.nombreOficina = nombreOficina;
	}
//	public String getLugar() {
//		return lugar;
//	}
//	public void setLugar(String lugar) {
//		this.lugar = lugar;
//	}
//	public TurnoDTO getTurno() {
//		return turno;
//	}
//	public void setTurno(TurnoDTO turno) {
//		this.turno = turno;
//	}
	public String getEstadoServicio() {
		return estadoServicio;
	}
	public void setEstadoServicio(String estadoServicio) {
		this.estadoServicio = estadoServicio;
	}
	public String getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	public String getUsuarioAlta() {
		return usuarioAlta;
	}
	public void setUsuarioAlta(String usuarioAlta) {
		this.usuarioAlta = usuarioAlta;
	}
//	public String getObservacion() {
//		return observacion;
//	}
//	public void setObservacion(String observacion) {
//		this.observacion = observacion;
//	}
//	public TramiteDTO getTramite() {
//		return tramite;
//	}
//	public void setTramite(TramiteDTO tramite) {
//		this.tramite = tramite;
//	}
//	public String getTelefono() {
//		return telefono;
//	}
//	public void setTelefono(String telefono) {
//		this.telefono = telefono;
//	}
//	public String getEmail() {
//		return email;
//	}
//	public void setEmail(String email) {
//		this.email = email;
//	}
//	public String getDetalleTramite() {
//		return detalleTramite;
//	}
//	public void setDetalleTramite(String detalleTramite) {
//		this.detalleTramite = detalleTramite;
//	}
//	public Float getLatitud() {
//		return latitud;
//	}
//	public void setLatitud(Float latitud) {
//		this.latitud = latitud;
//	}
//	public Float getLongitud() {
//		return longitud;
//	}
//	public void setLongitud(Float longitud) {
//		this.longitud = longitud;
//	}
//	public String getDescripcionLugar() {
//		return descripcionLugar;
//	}
//	public void setDescripcionLugar(String descripcionLugar) {
//		this.descripcionLugar = descripcionLugar;
//	}
	public List<FormularioDTO> getFormularios() {
		if(formularios == null)
			formularios = new ArrayList<FormularioDTO>();
		return formularios;
	}
	public void setFormularios(List<FormularioDTO> formularios) {
		this.formularios = formularios;
	}
//	public Long getIdEnte() {
//		return idEnte;
//	}
//	public void setIdEnte(Long idEnte) {
//		this.idEnte = idEnte;
//	}
//	public Long getIdTramite() {
//		return idTramite;
//	}
//	public void setIdTramite(Long idTramite) {
//		this.idTramite = idTramite;
//	}
//	public Boolean getEnviarNovedades() {
//		return enviarNovedades;
//	}
//	public void setEnviarNovedades(Boolean enviarNovedades) {
//		this.enviarNovedades = enviarNovedades;
//	}
	public String getSistema() {
		return sistema;
	}
	public void setSistema(String sistema) {
		this.sistema = sistema;
	}
	public String getTipoDeServicio() {
		return tipoDeServicio;
	}
	public void setTipoDeServicio(String tipoDeServicio) {
		this.tipoDeServicio = tipoDeServicio;
	}
	public List<PersonaSENASADTOLight> getAgentes() {
		if(agentes == null)
			agentes = new ArrayList<PersonaSENASADTOLight>();
		return agentes;
	}
	public void setAgentes(List<PersonaSENASADTOLight> agentes) {
		this.agentes = agentes;
	}
}