package gov.senasa.sas.data.dto;

import java.util.List;

public class ItemResultadoFormularioGenericoDTO extends ItemResultadoFormularioDTO {
	private List<DatoResultadoFormularioDTO> datos;

	public List<DatoResultadoFormularioDTO> getDatos() {
		return datos;
	}
	public void setDatos(List<DatoResultadoFormularioDTO> datos) {
		this.datos = datos;
	}
}