package gov.senasa.sas.data.dto;

import java.util.List;

public class ItemResultadoFormularioCompositeDTO extends ItemResultadoFormularioDTO {
	private List<ItemResultadoFormularioDTO> items;
	
	public List<ItemResultadoFormularioDTO> getItems() {
		return items;
	}
	public void setItems(List<ItemResultadoFormularioDTO> items) {
		this.items = items;
	}
}