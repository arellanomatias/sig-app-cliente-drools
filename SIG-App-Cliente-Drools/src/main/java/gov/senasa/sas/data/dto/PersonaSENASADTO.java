package gov.senasa.sas.data.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PersonaSENASADTO {
	private Long id;
	private String nombre;
	private String cuit;
	private Integer lpu;
	private String nombreUsuario;
	private String situacionDeRevista;
	private Date baja;
	private List<RolDTO> roles;
	
	public PersonaSENASADTO() {
		
	}
	
	public PersonaSENASADTO(Long id) {
		super();
		this.id = id;
	}
	
	public PersonaSENASADTO(String nombreUsuario) {
		super();
		this.nombreUsuario = nombreUsuario;
	}

	public PersonaSENASADTO(Long id, String nombre, String nombreUsuario) {
		this();
		this.setId(id);
		this.setNombre(nombreUsuario);
		this.setNombreUsuario(nombreUsuario);
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCuit() {
		return cuit;
	}
	public void setCuit(String cuit) {
		this.cuit = cuit;
	}
	public Integer getLpu() {
		return lpu;
	}
	public void setLpu(Integer lpu) {
		this.lpu = lpu;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public String getSituacionDeRevista() {
		return situacionDeRevista;
	}
	public void setSituacionDeRevista(String situacionDeRevista) {
		this.situacionDeRevista = situacionDeRevista;
	}
	public Date getBaja() {
		return baja;
	}
	public void setBaja(Date baja) {
		this.baja = baja;
	}
	public List<RolDTO> getRoles() {
		if(this.roles == null) {
			this.roles = new ArrayList<RolDTO>();
		}
		return roles;
	}
	public void setRoles(List<RolDTO> roles) {
		this.roles = roles;
	}

	

}