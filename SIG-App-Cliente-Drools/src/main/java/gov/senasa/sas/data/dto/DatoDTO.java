package gov.senasa.sas.data.dto;

public class DatoDTO {

	private Long id;
	private String valor;

	public DatoDTO() {
		super();
	}
	
	public DatoDTO(Long id, String valor) {
		super();
		this.id = id;
		this.valor = valor;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
}