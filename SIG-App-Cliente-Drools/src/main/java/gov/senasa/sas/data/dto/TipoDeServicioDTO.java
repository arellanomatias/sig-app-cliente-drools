package gov.senasa.sas.data.dto;

import java.util.ArrayList;
import java.util.List;

public class TipoDeServicioDTO {
	private Integer id;
	private String nombre;
	private List<FormularioDTO> formularios;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public List<FormularioDTO> getFormularios() {
		if(formularios == null)
			formularios = new ArrayList<FormularioDTO>();
		return formularios;
	}
	public void setFormularios(List<FormularioDTO> formularios) {
		this.formularios = formularios;
	}
}