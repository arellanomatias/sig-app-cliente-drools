package gov.senasa.sas.data.dto;

import java.util.Map;

/**
 * 
 * @author guillermo
 *
 */
public class ResultadoJSONDTO {

	private String indice;
	private Long resultadoFormularioId;
	private Boolean pendienteParaGuardar;
	private Boolean guardadoParcial;
	private Map<String,ResultadoItemsJSON>respuestas;
	private Map<String,Object>respuestasGrupo;
	private Long asignacionAgenteServicioId;
	
	public ResultadoJSONDTO(String indice, Boolean pendienteParaGuardar, Long asignacionAgenteServicioId) {
		this.setIndice(indice);
		this.setPendienteParaGuardar(pendienteParaGuardar);
		this.setGuardadoParcial(false);
		this.setAsignacionAgenteServicioId(asignacionAgenteServicioId);
	}
	
	public String getIndice() {
		return indice;
	}
	public void setIndice(String indice) {
		this.indice = indice;
	}
	
	public Map<String,ResultadoItemsJSON> getRespuestas() {
		return respuestas;
	}
	public void setRespuestas(Map<String,ResultadoItemsJSON> respuestas) {
		this.respuestas = respuestas;
	}

	public Long getResultadoFormularioId() {
		return resultadoFormularioId;
	}

	public void setResultadoFormularioId(Long resultadoFormularioId) {
		this.resultadoFormularioId = resultadoFormularioId;
	}

	public Boolean getPendienteParaGuardar() {
		return pendienteParaGuardar;
	}

	public void setPendienteParaGuardar(Boolean pendienteParaGuardar) {
		this.pendienteParaGuardar = pendienteParaGuardar;
	}

	public Boolean getGuardadoParcial() {
		return guardadoParcial;
	}

	public void setGuardadoParcial(Boolean guardadoParcial) {
		this.guardadoParcial = guardadoParcial;
	}

	public Long getAsignacionAgenteServicioId() {
		return asignacionAgenteServicioId;
	}

	public void setAsignacionAgenteServicioId(Long asignacionAgenteServicioId) {
		this.asignacionAgenteServicioId = asignacionAgenteServicioId;
	}

	public Map<String, Object> getRespuestasGrupo() {
		return respuestasGrupo;
	}

	public void setRespuestasGrupo(Map<String, Object> respuestasGrupo) {
		this.respuestasGrupo = respuestasGrupo;
	}
}