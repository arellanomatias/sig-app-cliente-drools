package gov.senasa.sas.data.dto;

import java.util.List;

public class AsignacionAgenteServicioDTO {
	private Long id;
	private PersonaSENASADTO agente;
	private ServicioDTO servicio;
	private String estadoAsignado;
	private List<ResultadoFormularioDTO> resultadosFormulario;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public PersonaSENASADTO getAgente() {
		return agente;
	}
	public void setAgente(PersonaSENASADTO agente) {
		this.agente = agente;
	}
	public ServicioDTO getServicio() {
		return servicio;
	}
	public void setServicio(ServicioDTO servicio) {
		this.servicio = servicio;
	}
	public String getEstadoAsignado() {
		return estadoAsignado;
	}
	public void setEstadoAsignado(String estadoAsignado) {
		this.estadoAsignado = estadoAsignado;
	}
	public List<ResultadoFormularioDTO> getResultadosFormulario() {
		return resultadosFormulario;
	}
	public void setResultadosFormulario(List<ResultadoFormularioDTO> resultadosFormulario) {
		this.resultadosFormulario = resultadosFormulario;
	}
}