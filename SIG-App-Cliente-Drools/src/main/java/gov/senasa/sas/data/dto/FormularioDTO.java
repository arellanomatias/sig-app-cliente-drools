package gov.senasa.sas.data.dto;

import java.util.List;

public class FormularioDTO {
	private Long id;
	private String nombre;
	private List<ItemFormularioDTO> items;
//	private Integer idProtocolo;
	private String nombreProtocolo;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public List<ItemFormularioDTO> getItems() {
		return items;
	}
	public void setItems(List<ItemFormularioDTO> items) {
		this.items = items;
	}
//	public Integer getIdProtocolo() {
//		return idProtocolo;
//	}
//	public void setIdProtocolo(Integer idProtocolo) {
//		this.idProtocolo = idProtocolo;
//	}
	public String getNombreProtocolo() {
		return nombreProtocolo;
	}
	public void setNombreProtocolo(String nombreProtocolo) {
		this.nombreProtocolo = nombreProtocolo;
	}
}