package gov.senasa.sas.data.dto;

public class DatoResultadoTipoDatoMultipleAppDTO {
	private Long id;
	private String valor;
	private boolean value;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public boolean isValue() {
		return value;
	}
	public void setValue(boolean value) {
		this.value = value;
	}
}