package gov.senasa.sas.data.dto;

import java.util.List;

public class ResultadoFormularioDTO {
	private Long id;
	private Long idFormulario;
//	private String nombreFormulario;
	private List<ItemResultadoFormularioDTO> items;
	private boolean modificacionHabilitada;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdFormulario() {
		return idFormulario;
	}
	public void setIdFormulario(Long idFormulario) {
		this.idFormulario = idFormulario;
	}
//	public String getNombreFormulario() {
//		return nombreFormulario;
//	}
//	public void setNombreFormulario(String nombreFormulario) {
//		this.nombreFormulario = nombreFormulario;
//	}
	public List<ItemResultadoFormularioDTO> getItems() {
		return items;
	}
	public void setItems(List<ItemResultadoFormularioDTO> items) {
		this.items = items;
	}
	public boolean isModificacionHabilitada() {
		return modificacionHabilitada;
	}
	public void setModificacionHabilitada(boolean modificacionHabilitada) {
		this.modificacionHabilitada = modificacionHabilitada;
	}
}