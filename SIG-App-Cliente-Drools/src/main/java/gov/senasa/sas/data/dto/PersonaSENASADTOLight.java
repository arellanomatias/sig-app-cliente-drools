package gov.senasa.sas.data.dto;

import java.util.Date;

public class PersonaSENASADTOLight {
	private Long id;
	private Long idAsignacion;
	private String nombre;
	private String cuit;
	private Integer lpu;
	private String nombreUsuario;
	private Date baja;
	
	public PersonaSENASADTOLight() {
		
	}
	
	public PersonaSENASADTOLight(Long id) {
		super();
		this.id = id;
	}
	
	public PersonaSENASADTOLight(String nombreUsuario) {
		super();
		this.nombreUsuario = nombreUsuario;
	}

	public PersonaSENASADTOLight(Long id, String nombre, String nombreUsuario) {
		this();
		this.setId(id);
		this.setNombre(nombreUsuario);
		this.setNombreUsuario(nombreUsuario);
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdAsignacion() {
		return idAsignacion;
	}

	public void setIdAsignacion(Long idAsignacion) {
		this.idAsignacion = idAsignacion;
	}

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCuit() {
		return cuit;
	}
	public void setCuit(String cuit) {
		this.cuit = cuit;
	}
	public Integer getLpu() {
		return lpu;
	}
	public void setLpu(Integer lpu) {
		this.lpu = lpu;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public Date getBaja() {
		return baja;
	}
	public void setBaja(Date baja) {
		this.baja = baja;
	}
}