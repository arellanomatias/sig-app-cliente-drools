package gov.senasa.sas.data.dto;

import java.util.Date;
import java.util.List;

public class RecorridoDTO {

	private Long id;
	private List<ActividadDTO> actividades;
	private Date fechaAlta;
	private Date inicio;
	private Date fin;
	private List<CoordenadaDTO> coordenadas;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<ActividadDTO> getActividades() {
		return actividades;
	}
	public void setActividades(List<ActividadDTO> actividades) {
		this.actividades = actividades;
	}
	public Date getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	public Date getInicio() {
		return inicio;
	}
	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}
	public Date getFin() {
		return fin;
	}
	public void setFin(Date fin) {
		this.fin = fin;
	}
	public List<CoordenadaDTO> getCoordenadas() {
		return coordenadas;
	}
	public void setCoordenadas(List<CoordenadaDTO> coordenadas) {
		this.coordenadas = coordenadas;
	}
	
	


	
	
}