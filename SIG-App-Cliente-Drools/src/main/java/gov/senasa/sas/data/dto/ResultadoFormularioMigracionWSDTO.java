package gov.senasa.sas.data.dto;

import java.util.ArrayList;
import java.util.List;

public class ResultadoFormularioMigracionWSDTO {
	private Long idFormulario;
//	private String nombreFormulario;
	private String fechaInicio;
	private String fechaCierre;
	private List<ItemResultadoFormularioMigracionWSDTO> items;
	
	public Long getIdFormulario() {
		return idFormulario;
	}
	public void setIdFormulario(Long idFormulario) {
		this.idFormulario = idFormulario;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getFechaCierre() {
		return fechaCierre;
	}
	public void setFechaCierre(String fechaCierre) {
		this.fechaCierre = fechaCierre;
	}
	//	public String getNombreFormulario() {
//		return nombreFormulario;
//	}
//	public void setNombreFormulario(String nombreFormulario) {
//		this.nombreFormulario = nombreFormulario;
//	}
	public List<ItemResultadoFormularioMigracionWSDTO> getItems() {
		if(items == null)
			items = new ArrayList<ItemResultadoFormularioMigracionWSDTO>();
		return items;
	}
	public void setItems(List<ItemResultadoFormularioMigracionWSDTO> items) {
		this.items = items;
	}
}