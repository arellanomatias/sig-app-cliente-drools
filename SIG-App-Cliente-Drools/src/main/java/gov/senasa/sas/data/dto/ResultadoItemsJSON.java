package gov.senasa.sas.data.dto;
/**
 * 
 * @author guillermo
 *
 */
public class ResultadoItemsJSON {

	private Long itemFormularioId;
	private String respuestas;
	private Long itemRespuesta;
	
	public ResultadoItemsJSON() {
		
	}
	
	public ResultadoItemsJSON(Long itemFormularioId, String respuestas) {
		this.setItemFormularioId(itemFormularioId);
		this.setRespuestas(respuestas);
	}
	
	public Long getItemFormularioId() {
		return itemFormularioId;
	}
	public void setItemFormularioId(Long itemFormularioId) {
		this.itemFormularioId = itemFormularioId;
	}
	public String getRespuestas() {
		return respuestas;
	}
	public void setRespuestas(String respuestas) {
		this.respuestas = respuestas;
	}

	public Long getItemRespuesta() {
		return itemRespuesta;
	}

	public void setItemRespuesta(Long itemRespuesta) {
		this.itemRespuesta = itemRespuesta;
	}
}
