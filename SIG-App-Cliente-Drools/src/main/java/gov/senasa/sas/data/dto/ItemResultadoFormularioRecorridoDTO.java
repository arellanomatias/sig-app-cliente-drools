package gov.senasa.sas.data.dto;

import java.util.List;

public class ItemResultadoFormularioRecorridoDTO {
	private Long id;
	private Long original;
	private Long idItemFormularioOriginal;
	private Long idItemFormulario;
	private List<DatoResultadoFormularioRecorridoDTO> datos;
	private Object value;
	
	public Long getIdItemFormulario() {
		return idItemFormulario;
	}
	public void setIdItemFormulario(Long idItemFormulario) {
		this.idItemFormulario = idItemFormulario;
	}
	public List<DatoResultadoFormularioRecorridoDTO> getDatos() {
		return datos;
	}
	public void setDatos(List<DatoResultadoFormularioRecorridoDTO> datos) {
		this.datos = datos;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getOriginal() {
		return original;
	}
	public void setOriginal(Long original) {
		this.original = original;
	}
	public Long getIdItemFormularioOriginal() {
		return idItemFormularioOriginal;
	}
	public void setIdItemFormularioOriginal(Long idItemFormularioOriginal) {
		this.idItemFormularioOriginal = idItemFormularioOriginal;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	
	public Long getIdIForm() {
		/*Se cambia para la version 3.0*/
		if(getIdItemFormularioOriginal() != null) return getIdItemFormularioOriginal();
		else if(getOriginal() != null) return getOriginal();
		else if(getId() != null) return getId();
		else if(getIdItemFormulario() != null) return getIdItemFormulario();
		
		/*Para versiones 2.8 y superiores*/
//		if(getOriginal() != null) return getOriginal();
//		else if(getId() != null) return getId();
		/*Para versiones 2.7 e inferiores*/
//		else if(getIdItemFormulario() != null) return getIdItemFormulario();
		return null;
	}
	
	public Object getRespuesta() {
		/*Para versiones 2.8 y superiores*/
		if(getValue() != null) return getValue();
		/*Para versiones 2.7 e inferiores*/
		if(getDatos() != null && !getDatos().isEmpty()) return getDatos().get(0).getRespuesta();
		return null;
	}
}