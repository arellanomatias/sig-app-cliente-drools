package gov.senasa.sas.data.dto;

public class DatoResultadoFormularioRecorridoDTO {
	private Object respuesta;
	private Object codigoRespuesta;
	
	public DatoResultadoFormularioRecorridoDTO() {
		super();
	}
	
	public DatoResultadoFormularioRecorridoDTO(Object respuesta) {
		super();
		this.respuesta = respuesta;
	}
	
	public Object getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(Object respuesta) {
		this.respuesta = respuesta;
	}
	public Object getCodigoRespuesta() {
		return codigoRespuesta;
	}
	public void setCodigoRespuesta(Object codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
}