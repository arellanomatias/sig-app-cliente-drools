package gov.senasa.sas.data.dto;

import java.util.ArrayList;
import java.util.List;

public class ItemFormularioCompositeDTO extends ItemFormularioDTO{
	private List<ItemFormularioDTO> items;
	private boolean permiteRepetir;
	private int copiaNumero = 0; //Se agrega para que la app sepa que es la copia original del grupo
	
	public void addItem(ItemFormularioDTO item) {
		if(item != null) {
			if(getItems() == null)
				setItems(new ArrayList<ItemFormularioDTO>());
			
			getItems().add(item);
		}
	}

	public List<ItemFormularioDTO> getItems() {
		return items;
	}
	public void setItems(List<ItemFormularioDTO> items) {
		this.items = items;
	}
	public boolean isPermiteRepetir() {
		return permiteRepetir;
	}
	public void setPermiteRepetir(boolean permiteRepetir) {
		this.permiteRepetir = permiteRepetir;
	}
	public int getCopiaNumero() {
		return copiaNumero;
	}
	public void setCopiaNumero(int copiaNumero) {
		this.copiaNumero = copiaNumero;
	}
}