package gov.senasa.sas.data.dto;

import java.util.ArrayList;
import java.util.List;

public class ItemResultadoFormularioCompositeMigracionWSDTO extends ItemResultadoFormularioMigracionWSDTO {
	private List<ItemResultadoFormularioMigracionWSDTO> items;
	
	public List<ItemResultadoFormularioMigracionWSDTO> getItems() {
		if(items == null)
			items = new ArrayList<ItemResultadoFormularioMigracionWSDTO>();
		return items;
	}
	public void setItems(List<ItemResultadoFormularioMigracionWSDTO> items) {
		this.items = items;
	}
}