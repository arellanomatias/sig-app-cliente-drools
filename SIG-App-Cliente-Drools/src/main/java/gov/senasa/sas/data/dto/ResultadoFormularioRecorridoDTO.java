package gov.senasa.sas.data.dto;

import java.util.List;

public class ResultadoFormularioRecorridoDTO {
	private Long id;
	private Long idFormulario;
	private List<ItemResultadoFormularioRecorridoDTO> items;
	private boolean modificado;
	private boolean modificacionHabilitada;
	private boolean valido;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdFormulario() {
		return idFormulario;
	}
	public void setIdFormulario(Long idFormulario) {
		this.idFormulario = idFormulario;
	}
	public List<ItemResultadoFormularioRecorridoDTO> getItems() {
		return items;
	}
	public void setItems(List<ItemResultadoFormularioRecorridoDTO> items) {
		this.items = items;
	}
	public boolean isModificado() {
		return modificado;
	}
	public void setModificado(boolean modificado) {
		this.modificado = modificado;
	}
	public boolean isModificacionHabilitada() {
		return modificacionHabilitada;
	}
	public void setModificacionHabilitada(boolean modificacionHabilitada) {
		this.modificacionHabilitada = modificacionHabilitada;
	}
	public boolean isValido() {
		return valido;
	}
	public void setValido(boolean valido) {
		this.valido = valido;
	}
}