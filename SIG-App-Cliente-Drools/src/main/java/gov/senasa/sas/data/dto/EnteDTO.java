package gov.senasa.sas.data.dto;

public class EnteDTO {
	protected Long id;	
	protected String identificador;
	protected String razonSocial;
	protected String tipoEnte;
	protected String tipo;
	protected Boolean baja;
	protected Boolean esFavorito;
	
	public EnteDTO() {
		super();
	}

	public EnteDTO(Long id, String identificador, String razonSocial, String tipo) {
		super();
		this.id = id;
		this.identificador = identificador;
		this.razonSocial = razonSocial;
		this.tipo = tipo;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public String getTipoEnte() {
		return tipoEnte;
	}
	public void setTipoEnte(String tipoEnte) {
		this.tipoEnte = tipoEnte;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public Boolean getBaja() {
		return baja;
	}
	public void setBaja(Boolean baja) {
		this.baja = baja;
	}
	public Boolean getEsFavorito() {
		return this.esFavorito;
	}
	public void setEsFavorito(Boolean esFavorito) {
		this.esFavorito = esFavorito;
	}

	public String getTipoEnteString() {
		if(this.getTipoEnte().equals("P")) {
			return "Persona";
		}
		if(this.getTipoEnte().equals("E")) {
			return "Establecimiento";
		}
		if(this.getTipoEnte().equals("O")) {
			return "Oficina";
		}
		return "Sin registrar";
	}
}