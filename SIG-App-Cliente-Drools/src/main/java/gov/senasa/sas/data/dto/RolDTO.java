package gov.senasa.sas.data.dto;

/**
 * 
 * @author guillermo
 *
 */
public class RolDTO {
	private Long id;
	private String nombre;
	private boolean habilitado = true;

	public RolDTO() {}
	
	public RolDTO(Long id, String nombre, boolean habilitado) {
		this.setId(id);
		this.setNombre(nombre);
		this.setHabilitado(habilitado);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean isHabilitado() {
		return habilitado;
	}

	public void setHabilitado(boolean habilitado) {
		this.habilitado = habilitado;
	}
}
