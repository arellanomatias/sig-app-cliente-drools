package gov.senasa.sas.data.dto;

import java.util.List;

public class ItemFormularioGenericoDTO extends ItemFormularioDTO {
	private Long idOrigenDato;
	private String nombreOrigenDato;
	private List<DatoDTO> datos;
	
	public Long getIdOrigenDato() {
		return idOrigenDato;
	}
	public void setIdOrigenDato(Long idOrigenDato) {
		this.idOrigenDato = idOrigenDato;
	}
	public String getNombreOrigenDato() {
		return nombreOrigenDato;
	}
	public void setNombreOrigenDato(String nombreOrigenDato) {
		this.nombreOrigenDato = nombreOrigenDato;
	}
	public List<DatoDTO> getDatos() {
		return datos;
	}
	public void setDatos(List<DatoDTO> datos) {
		this.datos = datos;
	}
}