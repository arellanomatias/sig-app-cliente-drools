package gov.senasa.sas.data.dto;

public class DatoResultadoFormularioDTO {
//	private Long id;
	private String respuesta;
	private Long codigoRespuesta;
	
//	public Long getId() {
//		return id;
//	}
//	public void setId(Long id) {
//		this.id = id;
//	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public Long getCodigoRespuesta() {
		return codigoRespuesta;
	}
	public void setCodigoRespuesta(Long codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
}