package gov.senasa.sas.data.dto;

import java.util.Date;

import gov.senasa.sas.model.enums.EstadoAsignacionEnum;

public class TurnoDTO {
	private Long id;
	private Date horaDesde;
	private Date horaHasta;
	private Date fecha;
	private String horaDesdeString;
	private String horaHastaString;
	private String fechaString;
	private String observacion;
	private EstadoAsignacionEnum estadoAsignacion;
	
	public TurnoDTO() {
		
	}
	
	public TurnoDTO(Long id, Date horaDesde, Date horaHasta, Date fecha, EstadoAsignacionEnum estadoAsignacion) {
		super();
		this.id = id;
		this.horaDesde = horaDesde;
		this.horaHasta = horaHasta;
		this.fecha = fecha;
		this.estadoAsignacion = estadoAsignacion;
	}

	public EstadoAsignacionEnum getEstadoAsignacion() {
		return estadoAsignacion;
	}
	public void setEstadoAsignacion(EstadoAsignacionEnum estadoAsignacion) {
		this.estadoAsignacion = estadoAsignacion;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getHoraDesde() {
		return horaDesde;
	}
	public void setHoraDesde(Date horaDesde) {
		this.horaDesde = horaDesde;
	}
	public Date getHoraHasta() {
		return horaHasta;
	}
	public void setHoraHasta(Date horaHasta) {
		this.horaHasta = horaHasta;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public String getHoraDesdeString() {
		return horaDesdeString;
	}
	public void setHoraDesdeString(String horaDesdeString) {
		this.horaDesdeString = horaDesdeString;
	}
	public String getHoraHastaString() {
		return horaHastaString;
	}
	public void setHoraHastaString(String horaHastaString) {
		this.horaHastaString = horaHastaString;
	}
	public String getFechaString() {
		return fechaString;
	}
	public void setFechaString(String fechaString) {
		this.fechaString = fechaString;
	}
}