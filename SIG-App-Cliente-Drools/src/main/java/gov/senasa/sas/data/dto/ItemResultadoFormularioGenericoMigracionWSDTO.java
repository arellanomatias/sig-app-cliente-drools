package gov.senasa.sas.data.dto;

import java.util.ArrayList;
import java.util.List;

public class ItemResultadoFormularioGenericoMigracionWSDTO extends ItemResultadoFormularioMigracionWSDTO {
	private List<DatoResultadoFormularioMigracionWSDTO> datos;
	
	public List<DatoResultadoFormularioMigracionWSDTO> getDatos() {
		if(datos == null)
			datos = new ArrayList<DatoResultadoFormularioMigracionWSDTO>();
		return datos;
	}
	public void setDatos(List<DatoResultadoFormularioMigracionWSDTO> datos) {
		this.datos = datos;
	}
}