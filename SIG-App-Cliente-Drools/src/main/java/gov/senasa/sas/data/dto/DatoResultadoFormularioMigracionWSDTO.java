package gov.senasa.sas.data.dto;

public class DatoResultadoFormularioMigracionWSDTO {
	private String respuesta;
	private Long codigoRespuesta;
	
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public Long getCodigoRespuesta() {
		return codigoRespuesta;
	}
	public void setCodigoRespuesta(Long codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
}