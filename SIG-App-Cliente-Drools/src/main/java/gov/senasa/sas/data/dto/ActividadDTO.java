package gov.senasa.sas.data.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ActividadDTO {

	private Long id;
	private Date inicio;
	private Date fin;
	private Date horasNetas;
	private Long idAsignacionAgenteServicio;
	private Integer idTipoServicio;
	private String detalle;
	private EnteDTO ente;
	private CoordenadaDTO coordenadaDTO;
	private List<String> cuves;
	private List<ResultadoFormularioRecorridoDTO> resultadoFormularios;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getInicio() {
		return inicio;
	}
	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}
	public Date getFin() {
		return fin;
	}
	public void setFin(Date fin) {
		this.fin = fin;
	}
	public Date getHorasNetas() {
		return horasNetas;
	}
	public void setHorasNetas(Date horasNetas) {
		this.horasNetas = horasNetas;
	}
	public String getDetalle() {
		return detalle;
	}
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	public Long getIdAsignacionAgenteServicio() {
		return idAsignacionAgenteServicio;
	}
	public void setIdAsignacionAgenteServicio(Long idAsignacionAgenteServicio) {
		this.idAsignacionAgenteServicio = idAsignacionAgenteServicio;
	}
	public Integer getIdTipoServicio() {
		return idTipoServicio;
	}
	public void setIdTipoServicio(Integer idTipoServicio) {
		this.idTipoServicio = idTipoServicio;
	}
	public CoordenadaDTO getCoordenadaDTO() {
		return coordenadaDTO;
	}
	public void setCoordenadaDTO(CoordenadaDTO coordenadaDTO) {
		this.coordenadaDTO = coordenadaDTO;
	}
	public List<String> getCuves() {
		if(this.cuves == null) {
			this.setCuves(new ArrayList<String>());
		}
		return cuves;
	}
	public void setCuves(List<String> cuves) {
		this.cuves = cuves;
	}
	public List<ResultadoFormularioRecorridoDTO> getResultadoFormularios() {
		return resultadoFormularios;
	}
	public void setResultadoFormularios(List<ResultadoFormularioRecorridoDTO> resultadoFormularios) {
		this.resultadoFormularios = resultadoFormularios;
	}
	public EnteDTO getEnte() {
		return ente;
	}
	public void setEnte(EnteDTO ente) {
		this.ente = ente;
	}
}