package gov.senasa.sas.data.dto;

public class ItemResultadoFormularioDTO {
	private Long idItemFormulario;
	
	public Long getIdItemFormulario() {
		return idItemFormulario;
	}
	public void setIdItemFormulario(Long idItemFormulario) {
		this.idItemFormulario = idItemFormulario;
	}
}