package gov.senasa.sas.data.dto;

import java.util.Date;

public class CoordenadaDTO {

//	private Long id;
	private String latitud;
	private String longitud;
//	private Date fechaAlta;
	private Date horaRegistro;
	
//	public Long getId() {
//		return id;
//	}
//	public void setId(Long id) {
//		this.id = id;
//	}
	public String getLatitud() {
		return latitud;
	}
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	public String getLongitud() {
		return longitud;
	}
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
//	public Date getFechaAlta() {
//		return fechaAlta;
//	}
//	public void setFechaAlta(Date fechaAlta) {
//		this.fechaAlta = fechaAlta;
//	}
	public Date getHoraRegistro() {
		return horaRegistro;
	}
	public void setHoraRegistro(Date horaRegistro) {
		this.horaRegistro = horaRegistro;
	}

	
	
}