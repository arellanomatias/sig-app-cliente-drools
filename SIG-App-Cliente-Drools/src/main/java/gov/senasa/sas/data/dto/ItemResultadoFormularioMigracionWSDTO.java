package gov.senasa.sas.data.dto;

public class ItemResultadoFormularioMigracionWSDTO {
	private Long idItemFormulario;
//	private String etiquetaItemFormulario;
	
	public Long getIdItemFormulario() {
		return idItemFormulario;
	}
	public void setIdItemFormulario(Long idItemFormulario) {
		this.idItemFormulario = idItemFormulario;
	}
//	public String getEtiquetaItemFormulario() {
//		return etiquetaItemFormulario;
//	}
//	public void setEtiquetaItemFormulario(String etiquetaItemFormulario) {
//		this.etiquetaItemFormulario = etiquetaItemFormulario;
//	}
}