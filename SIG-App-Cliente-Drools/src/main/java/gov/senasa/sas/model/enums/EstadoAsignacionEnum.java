package gov.senasa.sas.model.enums;

import java.io.Serializable;

public enum EstadoAsignacionEnum implements Serializable {

	ASIGNADO("Asignado"),
	ACEPTADO("Aceptado"),
	RECHAZADO("Rechazado"),
	EN_CURSO("En Curso"),
	FINALIZADO("Finalizado"),
	NO_ASIGNADO("No asignado");

	
	private EstadoAsignacionEnum(String text) {
		this.text = text;
	}

	private final String text;

	public String getText() {
		return text;
	}

}
