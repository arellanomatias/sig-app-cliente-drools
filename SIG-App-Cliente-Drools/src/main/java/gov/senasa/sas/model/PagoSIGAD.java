package gov.senasa.sas.model;

import java.io.Serializable;

import gov.senasa.sas.model.enums.EstadoPagoEnum;

public class PagoSIGAD implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;
	
	private Long idObligacionDePago;
	
	private Long idBoleta;
	
	private Integer estadoPago;
	
	private Boolean anulado;
	
	public PagoSIGAD() {
		
	}

	public PagoSIGAD(Long idObligacionDePago, Long idBoleta) {
		super();
		this.idObligacionDePago = idObligacionDePago;
		this.idBoleta = idBoleta;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdObligacionDePago() {
		return idObligacionDePago;
	}

	public void setIdObligacionDePago(Long idObligacionDePago) {
		this.idObligacionDePago = idObligacionDePago;
	}

	public Long getIdBoleta() {
		return idBoleta;
	}

	public void setIdBoleta(Long idBoleta) {
		this.idBoleta = idBoleta;
	}
	
	public Integer getEstadoPago() {
		if(estadoPago == null)
			estadoPago = 0;
		return estadoPago;
	}

	public void setEstadoPago(Integer estadoPago) {
		this.estadoPago = estadoPago;
	}
	
	public Boolean getAnulado() {
		return anulado;
	}

	public void setAnulado(Boolean anulado) {
		this.anulado = anulado;
	}

	public EstadoPagoEnum getEstadoPagoEnum() {
		return EstadoPagoEnum.values()[this.getEstadoPago()];
	}
	
}