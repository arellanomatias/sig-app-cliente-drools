package gov.senasa.sas.model.enums;

import java.io.Serializable;

public enum EstadoTurno implements Serializable {
	
	PENDIENTE("Propuesto por Cliente"), //Pendiente cliente
	PENDIENTE_CLIENTE("Pediente Cliente"),
	ACEPTADO("Aceptado"),
	RECHAZADO("Rechazado"),
	SIN_PROPUESTAS("Sin propuestas");
	
	private EstadoTurno(String text) {
		this.text = text;
	}

	private final String text;

	public String getText() {
		return text;
	}

}
