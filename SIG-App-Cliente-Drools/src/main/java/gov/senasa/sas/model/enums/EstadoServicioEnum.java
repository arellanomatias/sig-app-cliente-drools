package gov.senasa.sas.model.enums;

import java.io.Serializable;

public enum EstadoServicioEnum implements Serializable{

	
	INICIAL("Solicitado"), 
	TURNO_CONFIRMADO("Confirmado"),
	ASIGNADO("Asingnado") ,
	TERMINADO("Finalizado"),  
	RECHAZADO("Rechazado"),
	ELIMINADO("Eliminado"),
	EN_GESTION("En Gestion");

	
	
	private EstadoServicioEnum(String text) {
		this.text = text;
	}

	private final String text;

	public String getText() {
		return text;
	}

}
