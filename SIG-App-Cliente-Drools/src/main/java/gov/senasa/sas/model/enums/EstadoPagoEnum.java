package gov.senasa.sas.model.enums;

import java.io.Serializable;

public enum EstadoPagoEnum implements Serializable {
	
	PENDIENTE("Pendiente"),
	SALDADO("Saldado"),
	VENCIDO("Vencido");
	
	private EstadoPagoEnum(String text) {
		this.text = text;
	}

	private final String text;

	public String getText() {
		return text;
	}

}
