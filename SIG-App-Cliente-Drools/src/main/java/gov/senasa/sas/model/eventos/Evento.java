package gov.senasa.sas.model.eventos;

import java.io.Serializable;
import java.util.Date;


/**
 * Un evento es la lista de estados que puede tener un servicio
 * tipo es la clase o enum generador del evento
 * valor es el id del objeto u ordinal del enum
 * descripcion es la leyenda que se le agrega para mostrar.
 * */

public class Evento implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String tipo; // (class o enum del evento)
	private Long valor; // valor (id del objeto u ordinal del enum)
	private String descripcion;
	private Boolean notificado;
	private Date fecha;
	
	
	public Evento() {}
	
	public Evento(String tipo, Long valor, String descripcion) {
		this.tipo = tipo;
		this.valor = valor;
		this.descripcion = descripcion;
		this.notificado = false;
		this.fecha = new Date();
	}
	

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public Long getValor() {
		return valor;
	}
	public void setValor(Long valor) {
		this.valor = valor;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public Boolean getNotificado() {
		return notificado;
	}
	public void setNotificado(Boolean notificado) {
		this.notificado = notificado;
	}
	
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	
}
