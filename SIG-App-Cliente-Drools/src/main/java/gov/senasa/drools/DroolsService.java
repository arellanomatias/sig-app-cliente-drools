package gov.senasa.drools;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.Agenda;

import gov.senasa.sas.data.dto.ServicioDTO;

public class DroolsService {

	
	public static KieContainer kieConteiner() {
		KieServices ks = KieServices.Factory.get();
	    KieContainer kContainer = ks.getKieClasspathContainer();
	    return kContainer;
	}
	
	public static KieSession kieSession(String kSessionName) {
		return kieConteiner().newKieSession(kSessionName);
	}

	public static void calcularEstado(ServicioDTO servicio) {
		KieSession ks = kieSession("ksession-rules_calcular_evento");
		Agenda agenda = ks.getAgenda();
		agenda.getAgendaGroup( "estadosDTO" ).setFocus();
//		agenda.getAgendaGroup( "eventos" ).setFocus();
		ks.insert(servicio);
		ks.fireAllRules();

	}
	
	public static Boolean validarEliminacionServicio(ServicioDTO servicio) {
		calcularEstado(servicio);
		KieSession ks = kieSession("ksession-rules_servicio_validaciones");
		ks.setGlobal("$salida", false);
        ks.insert(servicio);
		ks.fireAllRules();
		return (Boolean)ks.getGlobal("$salida");
		
	}
}
