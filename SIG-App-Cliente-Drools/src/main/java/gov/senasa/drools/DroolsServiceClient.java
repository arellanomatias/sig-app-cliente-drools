package gov.senasa.drools;

import java.util.ArrayList;
import java.util.List;

import org.kie.api.KieServices;
import org.kie.api.command.BatchExecutionCommand;
import org.kie.api.command.Command;
import org.kie.api.command.KieCommands;
import org.kie.api.runtime.ExecutionResults;
import org.kie.server.api.marshalling.MarshallingFormat;
import org.kie.server.api.model.ServiceResponse;
import org.kie.server.client.CredentialsProvider;
import org.kie.server.client.KieServicesClient;
import org.kie.server.client.KieServicesConfiguration;
import org.kie.server.client.KieServicesFactory;
import org.kie.server.client.RuleServicesClient;
import org.kie.server.client.credentials.EnteredCredentialsProvider;


import gov.senasa.sas.data.dto.ServicioDTO;



public class DroolsServiceClient {

	private static final String KIE_SERVER_URL = "http://localhost:8080/kie-server/services/rest/server";

	private static final String USERNAME = "marellano";

	private static final String PASSWORD = "megapro1";

	private static final String CONTAINER_ID ="SAS_1.0.0-SNAPSHOT";

	
	public static KieServices kieServices() { 
		KieServices kieServices = KieServices.Factory.get();
		return kieServices;
	}

	
	public static KieServicesClient kieServicesClient() {
		CredentialsProvider credentialsProvider = new EnteredCredentialsProvider(USERNAME, PASSWORD);
		KieServicesConfiguration kieServicesConfig = KieServicesFactory.newRestConfiguration(KIE_SERVER_URL, credentialsProvider);
		kieServicesConfig.setMarshallingFormat(MarshallingFormat.JSON);
		KieServicesClient kieServicesClient = KieServicesFactory.newKieServicesClient(kieServicesConfig);
		return kieServicesClient;
	}
	



	
	

	public static ServicioDTO calcularEstado(ServicioDTO servicio) {
		// Retrieve the RuleServices Client.
		RuleServicesClient rulesClient = kieServicesClient().getServicesClient(RuleServicesClient.class);
		/*
		 * Create the list of commands that we want to fire against the rule engine. In this case we insert 2 objects, applicant and loan,
		 * and we trigger a ruleflow (with the StartProcess command).
		 */
		List<Command<?>> commands = new ArrayList();

		KieCommands commandFactory = kieServices().getCommands();
		//The identifiers that we provide in the insert commands can later be used to retrieve the object from the response.
		commands.add(commandFactory.newInsert(servicio, "ServivioDTO"));
		commands.add(commandFactory.newAgendaGroupSetFocus("estadosDTO" ));
//		commands.add(commandFactory.newAgendaGroupSetFocus("eventos" ));
		commands.add(commandFactory.newFireAllRules());
		BatchExecutionCommand batchExecutionCommand = commandFactory.newBatchExecution(commands);
		ServiceResponse<ExecutionResults> response = rulesClient.executeCommandsWithResults(CONTAINER_ID, batchExecutionCommand);
		ExecutionResults results = response.getResult();

		ServicioDTO resultServicioDTO = (ServicioDTO) results.getValue("ServivioDTO");
		System.out.println(resultServicioDTO.getEstadoServicio()+"please");
		return resultServicioDTO;

//		
//		KieSession ks = kieSession("ksession-rules_calcular_evento");
//		Agenda agenda = ks.getAgenda();
//		agenda.getAgendaGroup( "estados" ).setFocus();
//		agenda.getAgendaGroup( "eventos" ).setFocus();
//		ks.insert(servicio);
//		ks.fireAllRules();

	}
	
//	public static Boolean validarEliminacionServicio(Servicio servicio) {
//		calcularEstado(servicio);
//		KieSession ks = kieSession("ksession-rules_servicio_validaciones");
//		ks.setGlobal("$salida", false);
//        ks.insert(servicio);
//		ks.fireAllRules();
//		return (Boolean)ks.getGlobal("$salida");
//		
//	}
}
